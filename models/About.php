<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "about".
 *
 * @property integer $id
 * @property string $text
 * @property string $image_name
 */
class About extends ActiveRecord
{
    /**
     * @return string tableName
     */
    public static function tableName()
    {
        return 'about';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['title'],  'string', 'max' => 255],
            [['text'],  'string'],
            [['text','title'], 'required'],
            [['image_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title'=>'Заголовок',
            'text' => 'Текст',
            'image_name' => 'Фото',
        ];
    }

    public function getImage(){
        $defaultImageUrl='/web/uploads/images/default.jpg';
        $imageUrl = Yii::getAlias('@uploads').'/about/'.$this->image_name;
        if (file_exists($imageUrl)&& is_file($imageUrl)) {
            return '/web/uploads/about/'.$this->image_name;
        } else {
            return $defaultImageUrl;
        }
    }
}
