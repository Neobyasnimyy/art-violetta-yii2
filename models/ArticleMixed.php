<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "article_mixed".
 *
 * @property integer $id
 * @property string $title
 * @property integer $perent_id
 * @property string $description
 * @property integer $is_active
 */
class ArticleMixed extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_mixed';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'description','name'], 'required'],
            [['parent_id', 'is_active'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'parent_id' => 'Категория',
            'description' => 'Описание',
            'is_active' => 'Активность',
        ];
    }

    /**
     * @return array
     */
    public static function getParentList(){
        return [1=>'Этнография и Арт-терапия',2=>'Виды мастер классов',3=>'Особые дети'];
    }

    public function getParentName()
    {
        $list= self::getParentList();
        return $list[$this->is_active];
    }

    /**
     * @return array
     */
    public static function getStatusList(){
        return [''=>'все',0=>'off',1=>'on'];
    }

    public function getStatus()
    {
        $list= self::getStatusList();
        return $list[$this->is_active];
    }
}
