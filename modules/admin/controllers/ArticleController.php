<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Article;
use app\models\ArticleSearch;
use yii\base\DynamicModel;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\UploadImage;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\helpers\Url;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends AppAdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModelArticle = new ArticleSearch();
        $modelArticle = new Article();
        $uploadImageArticle = new UploadImage();
        $dataProviderArticle = $searchModelArticle->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModelArticle' => $searchModelArticle,
            'dataProviderArticle' => $dataProviderArticle,
            'modelArticle' => $modelArticle,
            'uploadImageArticle' => $uploadImageArticle,
            'openFormArticle' => false,
        ]);
    }

//    /**
//     * Displays a single Article model.
//     * @param integer $id
//     * @return mixed
//     */
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $modelArticle = new Article();
        $uploadImageArticle = new UploadImage();
        $searchModelArticle = new ArticleSearch();
        $dataProviderArticle = $searchModelArticle->search(Yii::$app->request->queryParams);


        if ($modelArticle->load(Yii::$app->request->post())) {
            $uploadImageArticle->image = UploadedFile::getInstance($uploadImageArticle, 'image');
//            var_dump($uploadImageArticle->image);
//            die();
            if ($uploadImageArticle->image != null) {
                // этот метот сохраняет изображение в папке с id категории и занимается валидацией
                if ($uploadImageArticle->validate() && $uploadImageArticle->upload('article')) {

                    $modelArticle->image_name = $uploadImageArticle->image->name;
                    $modelArticle->save();
                    Yii::$app->session->setFlash('success', 'Данные приняты');
                    return $this->redirect(['/admin/article']);
                }
            } else {
                $uploadImageArticle->addErrors(['image' => 'загрузите обложку']);
            }

            Yii::$app->session->setFlash('danger', 'Ошибка записи');
            $openFormArticle = true;
            return $this->render('index', [
                'modelArticle' => $modelArticle,
                'searchModelArticle' => $searchModelArticle,
                'dataProviderArticle' => $dataProviderArticle,
                'uploadImageArticle' => $uploadImageArticle,
                'openFormArticle' => $openFormArticle,
            ]);

        }

        return $this->redirect('/admin/article');


    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $uploadImageArticle = new UploadImage();
        $modelArticle = $this->findModel($id);


        if ($modelArticle->load(Yii::$app->request->post()) && $modelArticle->validate()) {

            $uploadImageArticle->image = UploadedFile::getInstance($uploadImageArticle, 'image');

            $oldImageName = $modelArticle->image_name;

            // если была загруженна новая картинка и успешно она загрузилась на сервер
            if (!empty($uploadImageArticle->image && $uploadImageArticle->upload('article'))) {
                // меняем в нашей модели Article поле image_name
                $modelArticle->image_name = $uploadImageArticle->image->name;
                // если была старая картинка удаляем
                if ($oldImageName != null) {
                    myDelete(Yii::getAlias('@uploads') . '/article/' . $oldImageName); // удаляем изображение с сервера
                }
            }else{
                // ошибка при обработке картинки фозвращаем форму
                Yii::$app->session->setFlash('danger', 'Ошибка загрузки обложки');
                return $this->render('update', [
                    'modelArticle' => $modelArticle,
                    'uploadImageArticle' => $uploadImageArticle,
                ]);
            }

            $modelArticle->update();
            // созданние одноразовых сообщений для пользователя(хранятся в сессии)
            Yii::$app->session->setFlash('success', 'Данные сохранены');
            return $this->redirect(['/admin/article']);
        }

        return $this->render('update', [
            'modelArticle' => $modelArticle,
            'uploadImageArticle' => $uploadImageArticle,
        ]);

    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $article = $this->findModel($id);
        myDelete(Yii::getAlias('@uploads') . '/article/' . $article->image_name); // удаляем изображение с сервера
        $article->delete();  // удаляем запись из базы данных
        Yii::$app->session->setFlash('success', 'Удаление прошло успешно');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}
