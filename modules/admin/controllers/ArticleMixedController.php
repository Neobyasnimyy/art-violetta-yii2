<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\ArticleMixed;
use app\models\ArticleMixedSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ArticleMixedController implements the CRUD actions for ArticleMixed model.
 */
class ArticleMixedController extends AppAdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ArticleMixed models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleMixedSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $parentIdList=ArticleMixed::getParentList();
        $parentIdListListForFilter = ['' => 'Все'] + $parentIdList;
        $statusList = ArticleMixed::getStatusList();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'parentIdList'=>$parentIdList,
            'parentIdListListForFilter'=>$parentIdListListForFilter,
            'statusList'=>$statusList,
        ]);
    }

    /**
     * Displays a single ArticleMixed model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ArticleMixed model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ArticleMixed();
        $parentIdList=ArticleMixed::getParentList();


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Данные приняты'); // созданние одноразовых сообщений для пользователя(хранятся в сессии)

            return $this->redirect(['index', ]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'parentIdList'=>$parentIdList,
            ]);
        }
    }

    /**
     * Updates an existing ArticleMixed model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $parentIdList= ArticleMixed::getParentList();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Данные сохранены. Вернутся к <a href='/admin/article-mixed'>списку статей</a>!");
            return $this->refresh();
        } else {
            return $this->render('update', [
                'model' => $model,
                'parentIdList'=>$parentIdList

            ]);
        }
    }

    /**
     * Deletes an existing ArticleMixed model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ArticleMixed model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ArticleMixed the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ArticleMixed::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
