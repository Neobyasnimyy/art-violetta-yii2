<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\ArticleMixed */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="article-mixed-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php
    $params = [
        'prompt' => 'Выбрать'
    ];
    echo $form->field($model, 'parent_id')->dropDownList($parentIdList, $params); ?>


    <?php echo $form->field($model, 'description')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 100,
            'imageUpload' => Url::to(['article/save-redactor-img']),
            'imageManagerJson' => Url::to(['article/images-get']),
            'plugins' => [
                'clips',
                'fullscreen',
                'imagemanager',
                'fontsize',
                'fontcolor',
            ]
        ]
    ]); ?>


    <?= $form->field($model, 'is_active')->radioList([
        '1' => 'Вкл',
        '0' => 'Выкл',
    ], (empty($model->isActive)) ? ['value' => '1'] : []) ?>

    <div class="form-group">
        <?= Html::a('Вернутся к списку статей', ['/admin/article-mixed'], ['class' => 'btn btn-primary text-left']) ?>
        <?= Html::submitButton( 'Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
