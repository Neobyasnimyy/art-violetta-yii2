<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ArticleMixed */

$this->title = 'Новая статья';

?>
<div class="article-mixed-create">
<hr>


    <div class="col-md-8 col-md-offset-2">
        <h2 class="text-center"><?= Html::encode($this->title) ?></h2>

        <?= $this->render('_form', [
            'model' => $model,
            'parentIdList'=>$parentIdList
        ]) ?>
    </div>


</div>
