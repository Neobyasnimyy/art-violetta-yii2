<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ArticleMixedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статьи';
?>
<div class="article-mixed-index">
    <hr>
    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo Yii::$app->session->getFlash('success'); ?>
        </div>
    <?php endif; ?>


    <h2 class="text-center"><?= Html::encode($this->title) ?></h2>

    <p class="text-right">
        <?= Html::a('Добавить статью', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{summary}\n{items}",
        'columns' => [
//            'id',
            'name',
            [
                'attribute' => 'parent_id',
                'label' => 'Категория',
                'format' => 'raw', // Возможные варианты: raw, html

                'value' => function ($data) {
                    return $data->getParentName();
                },
//                'headerOptions' => ['width' => '200'],
                'filter' => Html::dropDownList('ArticleMixedSearch[parent_id]', $searchModel->parent_id, $parentIdListListForFilter, ['class' => 'form-control']),
            ],
            [
                'attribute' => 'description',
                'format' => 'raw',
                'contentOptions' => ['class' => '', 'style' => ''],
                'content' => function ($data) {
                    $text = $data->description;
                    if (strlen($text) > 200) {
                        return Html::tag('div', substr($text, 0, 200) . '...', ['class' => ['div_class'], 'style' => [
                            // свойства каждого div
                        ]]);
                    } else {
                        return Html::tag('div', $data->description, ['class' => ['div_class'], 'style' => [
                            // свойства каждого div
                        ]]);
                    }
                },
            ],
            [
                'attribute' => 'is_active',
//                'filter' =>\app\models\ArticleMixed::getStatusList(),
                'filter' => Html::dropDownList('ArticleMixedSearch[is_active]', $searchModel->is_active, $statusList, ['class' => 'form-control']),

                'value' => 'status',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => "{update} <br>{delete}"
            ],
        ],
    ]); ?>

    <div class="text-center">
        <?php echo LinkPager::widget([
            'pagination' => $dataProvider->pagination,
        ]); ?>
    </div>

    <?php Pjax::end(); ?></div>
