<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ListView;
use yii\widgets\LinkPager;

$this->registerJsFile('/js/admin/article/index.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile("/css/admin/article/index.css");

/* @var $this yii\web\View */
/* @var $searchModel app\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Настройка событий';

?>
<div class="article-index">

    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <?php if (Yii::$app->session->hasFlash('success')): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo Yii::$app->session->getFlash('success'); ?>
                </div>
            <?php endif; ?>

            <!--    --><?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <div id="addArticle" style="display: <?php echo ($openFormArticle) ? 'block' : 'none'; ?>" class="row">
                <hr>
                <h2 class="text-center">Новое событие</h2>

                <?= $this->render('_form', [
                    'modelArticle' => $modelArticle,
                    'uploadImageArticle' => $uploadImageArticle,
                ]); ?>
            </div>

            <br>
        </div>
    </div>


    <div>
        <hr>
        <h2 class="text-center"><?= Html::encode($this->title) ?></h2>
        <p class="text-right">
            <?php echo Html::submitButton('Добавить событие', ['id' => 'openFormNewArticle', 'class' => 'btn btn-primary']) ?>
        </p>
        <?php echo $this->render('_gridView', [
            'dataProviderArticle' => $dataProviderArticle,
            'searchModelArticle' => $searchModelArticle,
        ]) ?>
    </div>


</div>