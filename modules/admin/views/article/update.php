<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Category */

$this->title = $modelArticle->title;

?>
<div class="article-update row">
    <div class="col-md-8 col-md-offset-2">
        <hr>
        <?php echo Html::a('Вернутся к списку новостей', ['/admin/article'], ['class' => 'btn btn-primary text-left']) ?>
        <h2 class="text-center"><?= Html::encode($this->title) ?></h2>

        <?php if (Yii::$app->session->hasFlash('danger')): ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo Yii::$app->session->getFlash('danger'); ?>
            </div>
        <?php endif; ?>

        <?= $this->render('_form', [
            'modelArticle' => $modelArticle,
            'uploadImageArticle' =>$uploadImageArticle,
        ]) ?>
    </div>




</div>
