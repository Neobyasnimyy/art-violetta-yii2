<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $modelCategory app\models\Category */

$this->title = 'Создание новой категории для галлереи';
$this->registerCssFile("/css/admin/category/create-category.css");

?>
<div class="categories-create row">
    <div class="col-md-8 col-md-offset-2">
        <hr>
        <h2 class="text-center"><?= Html::encode($this->title) ?></h2>

        <br>


        <?= $this->render('_form-category', [
            'modelCategory' => $modelCategory,
            'status' => '1',
        ]) ?>


    </div>


</div>
