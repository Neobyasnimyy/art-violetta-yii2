<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
$this->registerJsFile('/js/admin/category/update-category.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerCssFile("/css/admin/category/update-category.css");

$this->title = $modelCategory->name;

?>

<div class="category-update">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <hr>
            <?php if (Yii::$app->session->hasFlash('success')): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo Yii::$app->session->getFlash('success'); ?>
                </div>
            <?php endif; ?>


            <h2 class="text-center"><?= Html::encode($this->title) ?></h2>

            <?php echo Html::Button('Развернуть форму категории', [
                'class' => 'btn btn-primary',
                'id' => 'open-form-category',
                'style' => 'display: none']); ?>

            <div class="categories-update-form">
                <?= $this->render('_form-category', [
                    'modelCategory' => $modelCategory,
                ]) ?>
            </div>
        </div>

    </div>


    <hr>
    <h2 class="text-center">Список изображений в категории <?php echo Html::encode($this->title) ?></h2>
    <p class="text-right">
        <?php echo Html::a('Добавить Изображение', ['/admin/image/create', 'categoryId' => $modelCategory->id], ['class' => 'btn btn-primary']); ?>
    </p>

    <div>
        <?php echo $this->render('_gridViewImage', [
            'dataProviderImage' => $dataProviderImage,
            'searchModelImage' => $searchModelImage,
            'categoryListForFilter' => $categoryListForFilter,
            'categoryList' => $categoryList,
            'categoryId' => $modelCategory->id,
        ]) ?>
    </div>
</div>

