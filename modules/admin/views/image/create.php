<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Image */

$this->title = 'Добавление изображения';
?>
<div class="images-create row">
    <div class="col-md-8 col-md-offset-2">
        <hr>
        <h2 class="text-center">
            <?php echo Html::encode($this->title);
            if ($modelImage->id_category)
                echo ' в категорию "'.$categoryList[$modelImage->id_category].'"' ;
            ?>
        </h2>
        <br>
        <?= $this->render('_form', [
            'modelImage' => $modelImage,
            'categoryList' => $categoryList,
            'uploadImage'=>$uploadImage,
        ]) ?>
    </div>





</div>
