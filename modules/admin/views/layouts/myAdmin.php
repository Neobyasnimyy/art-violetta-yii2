<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AdminAsset;

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="ru"> <!--<![endif]-->

<!--<html lang="--><?php //echo Yii::$app->language ?><!--">-->
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Exo+2:300,300i,400,400i,500,600,700,800,900%7CSource+Sans+Pro:200,300&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <!-- <link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="img/favicon/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png"> -->

</head>
<body>
<?php $this->beginBody() ?>

<header>

    <div id="header" class=" wrapper position-relative">
        <div class="container position-relative">
            <div class="container sixteen columns">
                <div class="header-top">
                    <div class="header-top_social">
                        <ul>
                            <li>
                                <a href="https://vk.com/id916151" title="Вконтакте Виолетта Лашкул">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 50 50">
                                        <g class="svg_soc-v">
                                            <path style=" " d="M 39.59375 0 L 10.40625 0 C 4.667969 0 0 4.667969 0 10.402344 L 0 39.597656 C 0 45.332031 4.667969 50 10.40625 50 L 39.59375 50 C 45.332031 50 50 45.332031 50 39.597656 L 50 10.402344 C 50 4.667969 45.332031 0 39.59375 0 Z M 40.464844 34.921875 L 35.925781 34.984375 C 35.925781 34.984375 34.945313 35.171875 33.660156 34.308594 C 31.964844 33.167969 30.363281 30.203125 29.113281 30.585938 C 28.0625 30.90625 27.84375 32.117188 27.84375 33.355469 C 27.84375 33.800781 27.523438 34 26.679688 34 C 25.835938 34 25.019531 34 24.648438 34 C 21.902344 34 19.179688 33.875 16.210938 30.699219 C 12.007813 26.203125 8.097656 17.820313 8.097656 17.820313 C 8.097656 17.820313 7.878906 17.363281 8.117188 17.085938 C 8.386719 16.773438 9.113281 16.8125 9.113281 16.8125 L 13.972656 16.8125 C 13.972656 16.8125 14.429688 16.902344 14.761719 17.136719 C 15.03125 17.332031 15.183594 17.703125 15.183594 17.703125 C 15.183594 17.703125 15.96875 19.652344 17.007813 21.410156 C 19.039063 24.847656 19.984375 25.597656 20.671875 25.230469 C 21.679688 24.695313 21.375 20.375 21.375 20.375 C 21.375 20.375 21.394531 18.808594 20.871094 18.109375 C 20.464844 17.570313 19.699219 17.410156 19.363281 17.367188 C 19.089844 17.332031 19.535156 16.710938 20.117188 16.429688 C 20.992188 16.011719 22.535156 15.988281 24.363281 16.007813 C 25.785156 16.019531 26.195313 16.105469 26.75 16.238281 C 28.425781 16.632813 27.855469 18.164063 27.855469 21.832031 C 27.855469 23.007813 27.636719 24.660156 28.503906 25.210938 C 28.878906 25.445313 29.789063 25.242188 32.0625 21.457031 C 33.144531 19.664063 33.953125 17.554688 33.953125 17.554688 C 33.953125 17.554688 34.132813 17.226563 34.40625 17.0625 C 34.6875 16.898438 34.679688 16.902344 35.066406 16.902344 C 35.453125 16.902344 39.332031 16.871094 40.179688 16.871094 C 41.03125 16.871094 41.828125 16.859375 41.964844 17.371094 C 42.164063 18.105469 41.390625 19.746094 39.300781 22.476563 C 35.871094 26.949219 35.488281 26.53125 38.335938 29.121094 C 41.058594 31.59375 41.621094 32.796875 41.714844 32.945313 C 42.84375 34.777344 40.464844 34.921875 40.464844 34.921875 Z "/>
                                        </g>
                                    </svg>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/violetta.lachkul" title="Фейсбук Виолетта Лашкул">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 50 50" >
                                        <g class="svg_soc-f">
                                            <path style=" " d="M 40 0 L 10 0 C 4.484375 0 0 4.484375 0 10 L 0 40 C 0 45.515625 4.484375 50 10 50 L 40 50 C 45.515625 50 50 45.515625 50 40 L 50 10 C 50 4.484375 45.515625 0 40 0 Z M 39 17 L 36 17 C 33.855469 17 33 17.503906 33 19 L 33 22 L 39 22 L 38 28 L 33 28 L 33 48 L 26 48 L 26 28 L 23 28 L 23 22 L 26 22 L 26 19 C 26 14.324219 27.582031 11 33 11 C 35.902344 11 39 12 39 12 Z "/>
                                        </g>
                                    </svg>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="header-top_user">
                        <div class="header-top_user_search">
                            <a href="#" title="Поиск по сайту">
                                <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     width="50px" height="50px" viewBox="0 0 50 50" enable-background="new 0 0 50 50" xml:space="preserve">
<circle class="svg_search" fill="none" stroke="#10222B" stroke-miterlimit="10" cx="20.756" cy="19.936" r="18.001"/>
                                    <line class="svg_search" fill="none" stroke="#10222B" stroke-width="3" stroke-miterlimit="10" x1="33.269" y1="32.87" x2="48.464" y2="48.065"/>
</svg>

                            </a>
                        </div>
                        <?php if (Yii::$app->user->isGuest) : ?>
                            <div class="header-top_user_log">
                                <a href="/site/login" title="Войти">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         width="50px" height="50px" viewBox="0 0 50 50" enable-background="new 0 0 50 50" xml:space="preserve">
<circle class="svg_user" fill="#10222B" cx="24.918" cy="14.531" r="11.222"/>
                                        <path class="svg_user" fill="#10222B" d="M25.156,31.229c13.813-0.47,23.82,12.417,23.82,12.417v5.044H1.336v-5.142
	C1.336,43.549,8.031,31.81,25.156,31.229z"/>
</svg>

                                </a>
                            </div>

                        <?php else: ?>
                            <div class="header-top_user_log">
                                <a href="/admin/article" style="color: black"><span class="glyphicon glyphicon-cog"></span></a>
                                <?php
                                echo Html::beginForm(['/site/logout'], 'post',['style'=>'display:inline;margin-left:8px;'])
                                    . Html::submitButton(
                                         ' <span class="glyphicon glyphicon-log-out"></span>',
                                        ['class' => ' logout']
                                    )
                                    . Html::endForm()
                                ;?>
                            </div>
                        <?php endif ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="nav" class="wrapper position-relative">
        <div class="container position-relative">
            <div class="container sixteen columns">
                <div class="header-title">
                    <h1>Violetta Lashkul</h1>
                </div>
                <div class="header-nav">
                    <nav>
                        <ul>
                            <li><a href="/" title="Главная Виолетта Лашкул">Главная</a></li>
                            <li><a href="/gallery" title="Галерея Виолетта Лашкул">Галерея</a></li>
                            <li><a href="/article" title="Статьи Виолетта Лашкул">Статьи</a></li>
                            <li><a href="/music" title="Музыка Виолетта Лашкул">Музыка</a></li>
                            <li><a href="/site/about" title="Контакты Виолетта Лашкул">Контакты</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>

</header>


<div class="container content-wrapp">
    <div class="btn-group">
        <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle">Настройки <span class="caret"></span></button>
        <ul class="dropdown-menu">
            <li><a href="/admin/article-mixed">Статьи</a></li>
            <li><a href="/admin/article">События</a></li>
            <li><a href="/admin/category">Галерея</a></li>
            <li><a href="/admin/music">Музыка</a></li>
            <li><a href="/admin/about">Обо мне</a></li>
        </ul>
    </div>
    <?php echo $content ?>
</div>





<div class="hidden"></div>


<footer class="wrapper position-relative">
    <div class="container position-relative">
        <div class="container sixteen columns">
            <div class="footer-about">
                <ul>
                    <li class="first">Violetta Lashkul</li>
                    <li>художник- декоратор</li>
                    <li>арт терапевт</li>
                    <li>специальный- психолог</li>
                </ul>
            </div>
            <div class="footer-copyright">
                <p>Art -Violetta 2017 <span>© All Rights Reserved.</span></p>
            </div>
        </div>
    </div>
</footer>

<!-- Load Scripts -->

<!--[if lt IE 9]>
<!--<script src="/libs/html5shiv/es5-shim.min.js"></script>-->
<!--<script src="/libs/html5shiv/html5shiv.min.js"></script>-->
<!--<script src="/libs/html5shiv/html5shiv-printshiv.min.js"></script>-->
<!--<script src="/libs/respond/respond.min.js"></script>-->
<![endif]-->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
