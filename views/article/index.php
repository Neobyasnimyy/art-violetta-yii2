<?php
//debug($modelArticle)
use yii\widgets\LinkPager;

$this->title = "Статьи";
$this->registerCssFile("/css/articles.css");

?>


<section id="articles">

    <div class="articles wrapper position-relative">
        <div class="container position-relative">
            <div class="container sixteen columns">
                <?php foreach ($modelArticle as $article): ?>

                    <div class="articles_pub">
                        <div class="articles_pub_date">
                            <time>
                                <span><?php echo date("d", strtotime($article['data'])); ?></span><?php echo getMouth(date("m", strtotime($article['data']))); ?>
                            </time>
                        </div>
                        <div class="articles_pub_img"><img src="<?php echo $article['image']; ?>"
                                                           alt="<?php echo $article['title']; ?>"></div>
                        <div class="articles_pub_text">
                            <div class="articles_pub_text-info">
                                <h2><?php echo $article['title']; ?></h2>
                                <p>
                                    <?php
                                    $description = preg_replace('/(<p>|<\/p>)/i', '', $article['description']);
                                    $description = preg_replace('/<img(.*?)>/i', '', $description);
                                    // выведем все предложенные которые поместились до 300 символов
                                    if (mb_strlen($article['description']) > 400) {
                                        echo mb_substr($description, 0,
                                                mb_strripos(mb_substr($description, 0, 400), '. ')
                                            ) . ' ...';
                                    } else {
                                        echo $description;
                                    } ?>
                                </p>
                            </div>
                            <div class="articles_pub_text-more"><a href="/article/view?id=<?php echo $article['id'] ?>"
                                                                   title="<?php echo $article['title']; ?>">Читать далее</a></div>
                        </div>
                    </div>
                <?php endforeach; ?>


            </div>
        </div>
    </div>

</section>


<div class="wrapper position-relative">
    <div class="container position-relative">
        <div class="container sixteen columns">

            <div class="pagination-block">
                <?php echo LinkPager::widget([
                    'pagination' => $pages,
                    'registerLinkTags' => true,
//                    'firstPageLabel' => '',
//                    'lastPageLabel' => '',
//                    'prevPageLabel' => '',
//                    'nextPageLabel' => '',
                ]); ?>
            </div>

        </div>
    </div>
</div>
