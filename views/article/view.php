<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Article */

$this->title = $modelArticle->title;

$this->registerCssFile("/css/articles-post.css");
$this->registerJsFile('/js/article/view.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]);

?>

<section id="articles-post">

    <div class="articles-post wrapper position-relative">
        <div class="container position-relative">
            <div class="container sixteen columns">

                <div class="articles-post-top">
                    <div class="articles-post-top_title"><h2><?php if($modelArticle['title']) echo $modelArticle['title'];?></h2></div>
                    <div class="articles-post-top_img"><img src="<?php echo $modelArticle['image']; ?>" alt="<?php if($modelArticle['title']) echo $modelArticle['title'];?>"></div>
                </div>

                <div class="articles-post-text">
                    <article>
                        <?php echo $modelArticle['description'];?>
                    </article>

                </div>

            </div>
        </div>
    </div>

</section>

<div class="wrapper position-relative">
    <div class="container position-relative">
        <div class="container sixteen columns">

            <div class="Back">
<!--                <a href="#" onclick="history.back();return false;"></a>-->
                <a href="/article" ></a>

            </div>

        </div>
    </div>
</div>