<?php
//debug($images);
use yii\widgets\LinkPager;
use yii\helpers\Html;

$this->title = "Галлерея";
$this->registerCssFile("/css/gallery.css");

?>




<section id="gallery">

    <div class="wrapper position-relative">
        <div class="container position-relative">
            <div class="container sixteen columns">

                <div class="gallery">
                    <?php $i = 0;
                    foreach ($modelGallery as $item):
                        if ($i == 3) {
                            echo '<div class="Border"></div>';
                        }
                        $i++;
                        ?>

                        <div class="gallery-item">
                            <div class="gallery-item_img">
                                <?php if (!empty($item['images'])): ?>
                                    <a href="/gallery/view?id=<?php echo $item['id'] ?>"
                                       title="<?php echo $item['name'].' '. $item['genre'] ?>"
                                    >
                                        <img src="/web/uploads/images/<?php echo   $item['id'] . '/' .$item['images'][0]->image_path?>" alt="<?php echo $item['name'] ?>">
                                    </a>
                                <?php endif; ?>

                            </div>

                            <div class="gallery-item_title">
                                <a href="/gallery/view?id=<?php echo $item['id'] ?>"
                                   title="<?php echo $item['name'].' '. $item['genre'] ?>">
                                    <h2><?php echo $item['name'] ?></h2>
                                    <h3><?php echo $item['genre'] ?></h3>
                                </a>
                            </div>
                        </div>

                    <?php endforeach; ?>

                </div>

            </div>
        </div>
    </div>

</section>

<div class="wrapper position-relative">
    <div class="container position-relative">
        <div class="container sixteen columns">

            <div class="pagination-block">
                <?php echo LinkPager::widget([
                    'pagination' => $pages,
                    'registerLinkTags' => true,
//                    'firstPageLabel' => '',
//                    'lastPageLabel' => '',
//                    'prevPageLabel' => '',
//                    'nextPageLabel' => '',
                ]); ?>
            </div>

        </div>
    </div>
</div>