<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Violetta Lashkul';
$this->registerCssFile("/css/index.css");
$this->registerCssFile("/css/owl.carousel.css");

$this->registerJsFile('/js/owl.carousel.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]);

$script = <<< JS
     $('.owl-carousel').owlCarousel({
	    loop:true,
	    margin:0,
	    nav:true,
	    items:1,
	    stagePadding:0,
	    smartSpeed:1250,
	    autoplay:true,
	    animateOut: 'fadeOut',
	  	animateIn: 'fadeIn',
	    navText:0
	})
JS;

//маркер конца строки, обязательно сразу, без пробелов и табуляции
$this->registerJs($script, yii\web\View::POS_END);

?>

<?php if ($slider_up): ?>
    <section class="slider-top wrapper position-relative">
        <div class="container position-relative">
            <div class="container sixteen columns">

                <div class="owl-carousel">
                    <?php foreach ($slider_up as $images): ?>

                        <?php foreach ($images['images'] as $image): ?>
                            <div class="slider-top-01">
                                <?php if (!empty($image['name_for_slider'])): ?>
                                    <h3><?php echo Html::encode($image['name_for_slider']); ?></h3>
                                <?php endif; ?>
                                <img src="<?php echo '/uploads/images/' . $image['id_category'] . '/' . $image['image_path'] ?>"
                                     alt="<?php echo Html::encode($image['name_for_slider']); ?>">
                            </div>
                        <?php endforeach; ?>

                    <?php endforeach; ?>
                </div>

            </div>
        </div>
    </section>
<?php endif; ?>

