<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Вход';
$this->registerCssFile("/css/login.css");
//$this->registerLinkTag([
//    'rel' => 'stylesheet',
//    'href' => '//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css',
//]);

?>
<?php //if (Yii::$app->session->hasFlash('success')): ?>
<!--    <div class="alert alert-success alert-dismissable">-->
<!--        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>-->
<!--        --><?php //echo Yii::$app->session->getFlash('success'); ?>
<!--    </div>-->
<?php //endif; ?>

<section>
    <div class="wrapper position-relative">
        <div class="container position-relative">
            <div class="container sixteen columns">

                <div class="enter-form">

                    <h3>Войти в личный кабинет</h3>

                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'layout' => 'horizontal',
                        'fieldConfig' => [
                            'template' => "<div>{input}\n{error}</div>",
                        ],
                    ]); ?>


                    <?= $form->field($model, 'email')->input('email')->textInput(['placeholder'=>$model->getAttributeLabel('email'),'autofocus' => false])->label(false); ?>

                    <?= $form->field($model, 'password')->passwordInput(['placeholder'=>$model->getAttributeLabel('password')])->label(false) ?>


                    <div class="enter-form_bottom">
                        <?= $form->field($model, 'rememberMe')->checkbox([
                            'template' => "<div>{input}{label}</div>",
                        ]) ?>

                        <div class="form-group">
                            <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</section>


